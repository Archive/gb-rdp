#include <stdlib.h>
#include <gtk/gtk.h>

#include "gb-rdp/gb-rdp.h"
GtkWidget *window = NULL;

static void
connect_ready (GObject *source_object,
               GAsyncResult *result,
               gpointer user_data)
{
    GbRdpSession *session = GB_RDP_SESSION (source_object);
    gboolean success;
    GError *error = NULL;

    success = gb_rdp_session_connect_finish (session, result, &error);
    g_debug ("connect success: %d", success);
}

static void
monitors (GbRdpSession *session, GParamSpec *pspec, gpointer user_data)
{
    cairo_surface_t *primary;
    GArray *monitors;

    g_debug ("got monitors");

    primary = gb_rdp_session_get_primary (session);
    g_return_if_fail (primary);
    monitors = gb_rdp_session_get_monitors (session);
    g_return_if_fail (monitors);

    GbRdpWidget *widget = gb_rdp_widget_new (session, 0);
    gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (widget));
    gtk_widget_show (GTK_WIDGET (widget));
    gtk_widget_grab_focus (GTK_WIDGET (widget));
}


int
main (int argc, char* argv[])
{
    GbRdpSession *session;
    gtk_init (&argc, &argv);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect (window, "destroy", gtk_main_quit, NULL);
    gtk_widget_show (window);

    session = gb_rdp_session_new ();
    g_object_set (session,
                  "username", "user",
                  "password", "pass",
                  "hostname", argv[1],
                  "port", atoi(argv[2]),
                  NULL);
    g_signal_connect (session, "notify::monitors", G_CALLBACK (monitors), NULL);
    gb_rdp_session_connect_async (session, NULL, connect_ready, NULL);

    gtk_main ();

    return 0;
}
