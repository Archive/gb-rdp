public int main (string[] argv) {
    Gtk.init (ref argv);

    var window = new Gtk.Window ();
    window.destroy.connect (Gtk.main_quit);
    window.show ();

    var session = new GbRdp.Session ();
    session.username = "user";
    session.password = "pass";
    session.hostname = argv[1];
    session.notify["monitors"].connect (() => {
        debug ("received monitors");
        var widget = new GbRdp.Widget (session, 0);
        window.add (widget);
        widget.show ();
    });

    session.connect_async.begin (null);

    Gtk.main ();

    return 0;
}