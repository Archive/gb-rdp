/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_RDP_UTIL_H_
#define GB_RDP_UTIL_H_

#include <glib-object.h>

G_BEGIN_DECLS

const gchar* gb_rdp_get_version_string (void);

#define GB_RDP_RESERVED_PADDING (10 * sizeof(void*))

G_END_DECLS

#endif /* GB_RDP_UTIL_H_ */
