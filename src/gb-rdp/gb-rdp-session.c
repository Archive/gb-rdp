/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <gdk/gdk.h>
#include <gio/gio.h>
#include <cairo-gobject.h>

#include "gb-rdp-priv.h"
#include "gb-rdp-session.h"

enum {
    DISCONNECTED,
    INVALIDATE,
    LAST_SIGNAL
};

enum  {
    PROP_0,
    PROP_HOSTNAME,
    PROP_PORT,
    PROP_USERNAME,
    PROP_PASSWORD,
    PROP_DOMAIN,
    PROP_PRIMARY,
    PROP_MONITORS,
    PROP_CURSOR,
};

G_DEFINE_TYPE (GbRdpSession, gb_rdp_session, G_TYPE_OBJECT)

static guint signals[LAST_SIGNAL] = { 0 };

typedef struct _GbRdpSource GbRdpSource;

struct _GbRdpSessionPrivate
{
    freerdp *instance;
    GbRdpSource *source;
    HCLRCONV clrconv;
    cairo_surface_t *primary;
    GArray *monitors;
    GdkCursor *cursor;
    gulong post_connect;
};

typedef struct _GbRdpPointer GbRdpPointer;

struct _GbRdpPointer
{
    rdpPointer _p;

    GdkCursor *cursor;
};

typedef struct _GbRdpContext GbRdpContext;

struct _GbRdpContext
{
    rdpContext _p;

    GbRdpSession *session;
};

struct _GbRdpSource
{
    GSource source;

    GbRdpSession *session;
    GArray *pollfds;
    gboolean error;
};

static gboolean
gb_rdp_source_prepare (GSource  *source,
                       gint     *timeout)
{
    GbRdpSource *src = (GbRdpSource *)source;
    int read_count = 0;
    int write_count = 0;
    // freerdp: why 32?
    void* read_fds[32] = { 0, };
    void* write_fds[32] = { 0, };
    int index;
    freerdp *instance = src->session->priv->instance;
    rdpChannels *channels = instance->context->channels;
    GPollFD pollfd = { 0, };

    g_return_val_if_fail (!src->error, TRUE);

    // glib todo: clear all pollfd at once
    for (index = 0; index < src->pollfds->len; index++) {
        GPollFD *pfd = &g_array_index (src->pollfds, GPollFD, index);
        g_source_remove_poll (source, pfd);
    }

    src->pollfds = g_array_set_size (src->pollfds, 0);

    // freerdp: simpler way?
    if (!freerdp_get_fds (instance, read_fds, &read_count, write_fds, &write_count)) {
        src->error = TRUE;
        return TRUE;
    }

    if (!freerdp_channels_get_fds (channels, instance, read_fds, &read_count, write_fds, &write_count)) {
        src->error = TRUE;
        return TRUE;
    }

    for (index = 0; index < read_count; index++) {
        pollfd.fd = (int) (read_fds[index]);
        pollfd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;
        g_array_append_val (src->pollfds, pollfd);
    }

    for (index = 0; index < write_count; index++) {
        pollfd.fd = (int) (write_fds[index]);
        pollfd.events = G_IO_OUT | G_IO_ERR;
        g_array_append_val (src->pollfds, pollfd);
    }

    for (index = 0; index < src->pollfds->len; index++) {
        GPollFD *pfd = &g_array_index (src->pollfds, GPollFD, index);
        g_source_add_poll (source, pfd);
    }

    return FALSE;
}

static gboolean
gb_rdp_source_check (GSource  *source)
{
    GbRdpSource *src = (GbRdpSource *)source;
    freerdp *instance = src->session->priv->instance;
    rdpChannels *channels = instance->context->channels;

    g_return_val_if_fail (!src->error, TRUE);

    if (freerdp_shall_disconnect (instance)) {
        g_debug ("shall disconnect");
        src->error = TRUE;
        return TRUE;
    }

    if (!freerdp_check_fds (instance)) {
        src->error = TRUE;
        return TRUE;
    }

    if (!freerdp_channels_check_fds (channels, instance)) {
        src->error = TRUE;
        return TRUE;
    }

    return FALSE;
}

static void
monitor_ready (GbRdpSession *self, rdpChannels* channels, freerdp* instance)
{
    g_debug ("monitor ready");
}

static gboolean
gb_rdp_source_dispatch (GSource     *source,
                        GSourceFunc  callback,
                        gpointer     user_data)

{
    GbRdpSource *src = (GbRdpSource *)source;
    freerdp *instance = src->session->priv->instance;
    rdpChannels *channels = instance->context->channels;
    RDP_EVENT* event;

    if (src->error) {
        int error = freerdp_error_info (instance);
        g_debug ("freerdp error: %d", error);

        g_signal_emit (src->session, signals[DISCONNECTED], 0, error);

        return FALSE;
    }

    event = freerdp_channels_pop_event (channels);
    if (!event)
        return TRUE;

    switch (event->event_class) {
    case RDP_EVENT_CLASS_RAIL:
        break;
    case RDP_EVENT_CLASS_TSMF:
        break;
    case RDP_EVENT_CLASS_CLIPRDR:
        break;

    default:
        g_critical ("Unknown event class: %d", event->event_class);
        break;
    }

    switch (event->event_type) {
    /* FIXME: uncomment when clipboard unbreak */
    case 1: /* RDP_EVENT_TYPE_CB_MONITOR_READY: */
        monitor_ready (src->session, channels, instance);
        break;

    default:
        g_critical ("Unknown event type: %d", event->event_type);
        break;
    }

    freerdp_event_free (event);

    return TRUE;
}

static void
gb_rdp_source_finalize (GSource *source)
{
    GbRdpSource *src = (GbRdpSource *)source;

    g_clear_object (&src->session);
    g_clear_pointer (&src->pollfds, g_array_unref);
}



static GSourceFuncs gb_rdp_source_funcs = {
    .prepare = gb_rdp_source_prepare,
    .check = gb_rdp_source_check,
    .dispatch = gb_rdp_source_dispatch,
    .finalize = gb_rdp_source_finalize,
};

static GbRdpSource*
gb_rdp_source_new (GbRdpSession *session)
{
    GSource *source = g_source_new (&gb_rdp_source_funcs, sizeof (GbRdpSource));
    GbRdpSource *src = (GbRdpSource *)source;

    g_source_set_name (source, "GbRdp");
    src->session = g_object_ref (session);
    src->pollfds = g_array_new (FALSE, TRUE, sizeof (GPollFD));

    return src;
}

static void
gb_rdp_session_update_source (GbRdpSession *self)
{
    guint id;

    // FIXME: it would be nice to reuse the same source
    g_clear_pointer (&self->priv->source, g_source_unref);
    self->priv->source = gb_rdp_source_new (self);
    id = g_source_attach ((GSource *)self->priv->source, NULL);
    g_debug ("attached gbrdp source %u", id);
}

GbRdpSession*
gb_rdp_session_new (void)
{
    return g_object_new (GB_RDP_TYPE_SESSION, NULL);
}

gboolean
gb_rdp_session_connect_finish (GbRdpSession *self,
                               GAsyncResult *result,
                               GError **error)
{
    GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (result);
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), FALSE);

    g_return_val_if_fail (
        g_simple_async_result_is_valid (result,
                                        G_OBJECT (self),
                                        gb_rdp_session_connect_async),
        FALSE);

    if (g_simple_async_result_propagate_error (simple, error))
        return FALSE;

    return g_simple_async_result_get_op_res_gboolean (simple);

}

static void
connect_async_thread (GSimpleAsyncResult *simple,
                      GObject *object,
                      GCancellable *cancellable)
{
    GbRdpSession *self = GB_RDP_SESSION (object);
    gboolean result;

    result = freerdp_connect (self->priv->instance);
    if (result)
        gb_rdp_session_update_source (self);

    g_simple_async_result_set_op_res_gboolean (simple, result);
}

void
gb_rdp_session_connect_async (GbRdpSession *self,
                              GCancellable *cancellable,
                              GAsyncReadyCallback callback,
                              gpointer user_data)
{
    GSimpleAsyncResult *simple;

    g_return_if_fail (GB_RDP_IS_SESSION (self));

    simple = g_simple_async_result_new (G_OBJECT (self),
                                        callback,
                                        user_data,
                                        gb_rdp_session_connect_async);

    g_simple_async_result_run_in_thread (simple,
                                         connect_async_thread,
                                         G_PRIORITY_DEFAULT,
                                         cancellable);
    g_object_unref (simple);
}

gboolean
gb_rdp_session_disconnect (GbRdpSession *self, GError **error)
{
    gboolean result;

    g_return_val_if_fail (GB_RDP_IS_SESSION (self), FALSE);

    result = freerdp_disconnect (self->priv->instance);

    return result;
}

static GbRdpSession *
self_from_instance (freerdp *instance)
{
    return ((GbRdpContext *)instance->context)->session;
}

static gboolean
pre_connect (freerdp *instance)
{
    rdpSettings *settings = instance->settings;
    rdpChannels *channels = instance->context->channels;
    gboolean bitmap_cache = settings->bitmap_cache;
    GbRdpSessionPrivate *s = self_from_instance (instance)->priv;

    g_debug ("%s", G_STRLOC);

    settings->os_major_type = OSMAJORTYPE_UNSPECIFIED;
#ifdef G_OS_UNIX
    settings->os_major_type = OSMAJORTYPE_UNIX;
#endif
#ifdef G_OS_WIN32
    settings->os_major_type = OSMAJORTYPE_WINDOWS;
#endif

    settings->os_minor_type = OSMINORTYPE_UNSPECIFIED;
#ifdef GDK_WINDOWING_X11
    settings->os_minor_type = OSMINORTYPE_NATIVE_XSERVER;
#endif
#ifdef GDK_WINDOWING_WIN32
    settings->os_minor_type = OSMINORTYPE_WINDOWS_NT;
#endif

    settings->order_support[NEG_DSTBLT_INDEX] = TRUE;
    settings->order_support[NEG_PATBLT_INDEX] = TRUE;
    settings->order_support[NEG_SCRBLT_INDEX] = TRUE;
    settings->order_support[NEG_OPAQUE_RECT_INDEX] = TRUE;
    settings->order_support[NEG_DRAWNINEGRID_INDEX] = FALSE;
    settings->order_support[NEG_MULTIDSTBLT_INDEX] = FALSE;
    settings->order_support[NEG_MULTIPATBLT_INDEX] = FALSE;
    settings->order_support[NEG_MULTISCRBLT_INDEX] = FALSE;
    settings->order_support[NEG_MULTIOPAQUERECT_INDEX] = TRUE;
    settings->order_support[NEG_MULTI_DRAWNINEGRID_INDEX] = FALSE;
    settings->order_support[NEG_LINETO_INDEX] = TRUE;
    settings->order_support[NEG_POLYLINE_INDEX] = TRUE;
    settings->order_support[NEG_MEMBLT_INDEX] = bitmap_cache;

    settings->order_support[NEG_MEM3BLT_INDEX] = (settings->sw_gdi) ? TRUE : FALSE;

    settings->order_support[NEG_MEMBLT_V2_INDEX] = bitmap_cache;
    settings->order_support[NEG_MEM3BLT_V2_INDEX] = FALSE;
    settings->order_support[NEG_SAVEBITMAP_INDEX] = FALSE;
    settings->order_support[NEG_GLYPH_INDEX_INDEX] = TRUE;
    settings->order_support[NEG_FAST_INDEX_INDEX] = TRUE;
    settings->order_support[NEG_FAST_GLYPH_INDEX] = TRUE;

    settings->order_support[NEG_POLYGON_SC_INDEX] = (settings->sw_gdi) ? FALSE : TRUE;
    settings->order_support[NEG_POLYGON_CB_INDEX] = (settings->sw_gdi) ? FALSE : TRUE;

    settings->order_support[NEG_ELLIPSE_SC_INDEX] = FALSE;
    settings->order_support[NEG_ELLIPSE_CB_INDEX] = FALSE;

    freerdp_channels_pre_connect (channels, instance);
    s->clrconv = freerdp_clrconv_new (CLRCONV_ALPHA);

    return TRUE;
}

static void
pointer_new (GbRdpContext *context, GbRdpPointer *pointer)
{
    rdpPointer *p = (rdpPointer *)pointer;
    guchar *pixels = malloc (p->width * p->height * 4);
    GbRdpSessionPrivate *s = context->session->priv;

    if ((p->andMaskData != 0) && (p->xorMaskData != 0))	{
        freerdp_alpha_cursor_convert ((UINT8*) (pixels), p->xorMaskData, p->andMaskData,
                                      p->width, p->height, p->xorBpp, s->clrconv);
    }
    cairo_surface_t *surface = cairo_image_surface_create_for_data (pixels, CAIRO_FORMAT_ARGB32, p->width, p->height, cairo_format_stride_for_width (CAIRO_FORMAT_ARGB32, p->width));
    GdkPixbuf *pixbuf = gdk_pixbuf_get_from_surface (surface, 0, 0, p->width, p->height);
    cairo_surface_destroy (surface);

    pointer->cursor = gdk_cursor_new_from_pixbuf (gdk_display_get_default (),
                                                  pixbuf, p->xPos, p->yPos);

}

static void
pointer_free (GbRdpContext *context, GbRdpPointer *pointer)
{
    g_clear_object (&pointer->cursor);
}

static void
gb_rdp_session_set_cursor (GbRdpSession *self, GdkCursor *cursor)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));

    if (self->priv->cursor == cursor)
        return;

    g_clear_object (&self->priv->cursor);
    self->priv->cursor = cursor ? g_object_ref (cursor) : NULL;
    g_object_notify (G_OBJECT (self), "cursor");
}

static void
pointer_set (GbRdpContext *context, GbRdpPointer *pointer)
{
    gb_rdp_session_set_cursor (context->session, pointer->cursor);
}

static void
pointer_setnull (GbRdpContext *context)
{
    GdkCursor *cursor = gdk_cursor_new (GDK_BLANK_CURSOR);

    gb_rdp_session_set_cursor (context->session, cursor);
    g_object_unref (cursor);
}

static void
pointer_setdefault (GbRdpContext *context)
{
    gb_rdp_session_set_cursor (context->session, NULL);
}


static void
register_graphics (rdpGraphics *graphics)
{
    rdpPointer pointer = { 0, };

    pointer.size = sizeof (GbRdpPointer);
    pointer.New = (pPointer_New)pointer_new;
    pointer.Free = (pPointer_Free)pointer_free;
    pointer.Set = (pPointer_Set)pointer_set;
    pointer.SetNull = (pPointer_SetNull)pointer_setnull;
    pointer.SetDefault = (pPointer_SetDefault)pointer_setdefault;

    graphics_register_pointer (graphics, &pointer);
}

static void
sw_begin_paint (GbRdpContext* context)
{
    rdpGdi *gdi = ((rdpContext*)context)->gdi;

    g_debug ("%s", G_STRLOC);

    gdi->primary->hdc->hwnd->invalid->null = 1;
    gdi->primary->hdc->hwnd->ninvalid = 0;
}

static void
sw_end_paint (GbRdpContext* context)
{
    rdpGdi *gdi = ((rdpContext*)context)->gdi;
    int i;
    int ninvalid;
    HGDI_RGN cinvalid;
    cairo_region_t *region;
    cairo_rectangle_int_t rect;

    g_debug ("%s", G_STRLOC);

    /* FIXME if (xfi->remote_app) */
    if (gdi->primary->hdc->hwnd->ninvalid < 1)
        return;

    region = cairo_region_create ();
    ninvalid = gdi->primary->hdc->hwnd->ninvalid;
    cinvalid = gdi->primary->hdc->hwnd->cinvalid;

    for (i = 0; i < ninvalid; i++) {
        rect.x = cinvalid[i].x;
        rect.y = cinvalid[i].y;
        rect.width = cinvalid[i].w;
        rect.height = cinvalid[i].h;

        cairo_region_union_rectangle (region, &rect);
    }

    g_signal_emit (context->session, signals[INVALIDATE], 0, region);

    cairo_region_destroy (region);
}

static void
sw_desktop_resize (GbRdpContext* context)
{
    g_debug ("%s", G_STRLOC);
}

static gboolean
post_connect_idle (GbRdpSession *self)
{
    GbRdpSessionPrivate *p = self->priv;
    freerdp *instance = p->instance;

    g_return_val_if_fail (instance->settings->sw_gdi, FALSE);
    g_debug ("%s", G_STRLOC);

    register_graphics (instance->context->graphics);

    {
        int format = CAIRO_FORMAT_RGB24;
        UINT32 flags = CLRCONV_ALPHA | CLRBUF_32BPP;
        int width = instance->settings->width;
        int height = instance->settings->height;
        int stride = cairo_format_stride_for_width (format, width);

        gdi_init (instance, flags, malloc (stride * height));

        rdpGdi *gdi = instance->context->gdi;
        self->priv->primary =
            cairo_image_surface_create_for_data (gdi->primary_buffer, format,
                                                 width, height, stride);
        g_object_notify (G_OBJECT (self), "primary");

        GArray *monitors = self->priv->monitors;
        g_array_set_size (monitors, 1);
        GbRdpMonitorConfig *config = &g_array_index (monitors, GbRdpMonitorConfig, 0);
        config->id = 0;
        config->rectangle.x = config->rectangle.y = 0;
        config->rectangle.width = width;
        config->rectangle.height = height;
        g_object_notify (G_OBJECT (self), "monitors");
    }

    instance->update->BeginPaint = (pBeginPaint)sw_begin_paint;
    instance->update->EndPaint = (pEndPaint)sw_end_paint;
    instance->update->DesktopResize = (pDesktopResize)sw_desktop_resize;

    pointer_cache_register_callbacks (instance->update);

    /* instance->context->rail = rail_new(instance->settings); */
    /* rail_register_update_callbacks(instance->context->rail, instance->update); */
    /* xf_rail_register_callbacks(xfi, instance->context->rail); */

    /* sse2 stuff... */

    freerdp_channels_post_connect (instance->context->channels, instance);

    /* xf_tsmf_init(xfi, xv_port); */

    /* xf_cliprdr_init(xfi, channels); */

    /* FIXME: this API is really weird and seems to be global.. */
    instance->settings->kbd_layout =
        freerdp_keyboard_init (instance->settings->kbd_layout);

    p->post_connect = 0;
    return FALSE;
}

static gboolean
post_connect (freerdp *instance)
{
    GbRdpSession *self = self_from_instance (instance);
    GbRdpSessionPrivate *p = self->priv;

    g_return_val_if_fail (p->post_connect == 0, FALSE);
    p->post_connect = g_idle_add ((GSourceFunc)post_connect_idle, self);
    return TRUE;
}

/** Callback set in the rdp_freerdp structure, and used to get the user's password,
 *  if required to establish the connection.
 *  This function is actually called in credssp_ntlmssp_client_init()
 *  @see rdp_server_accept_nego() and rdp_check_fds()
 *  @param instance - pointer to the rdp_freerdp structure that contains the connection settings
 *  @param username - unused
 *  @param password - on return: pointer to a character string that will be filled by the password entered by the user.
 *  				  Note that this character string will be allocated inside the function, and needs to be deallocated by the caller
 *  				  using xfree(), even in case this function fails.
 *  @param domain - unused
 *  @return TRUE if a password was successfully entered. See freerdp_passphrase_read() for more details.
 */
static gboolean
authenticate (freerdp* instance, char** username, char** password, char** domain)
{
    g_debug ("%s", G_STRLOC);

    /* *password = xmalloc(password_size * sizeof(char)); */
    /* if (freerdp_passphrase_read("Password: ", *password, password_size, instance->settings->from_stdin) == NULL) */
    /*     return FALSE; */

    return TRUE;
}

/** Callback set in the rdp_freerdp structure, and used to make a certificate validation
 *  when the connection requires it.
 *  This function will actually be called by tls_verify_certificate().
 *  @see rdp_client_connect() and tls_connect()
 *  @param instance - pointer to the rdp_freerdp structure that contains the connection settings
 *  @param subject
 *  @param issuer
 *  @param fingerprint
 *  @return TRUE if the certificate is trusted. FALSE otherwise.
 */
static gboolean
verify_certificate (freerdp *instance, char *subject, char *issuer, char *fingerprint)
{
    g_debug ("The above X.509 certificate could not be verified, possibly because you do not have "
             "the CA certificate in your certificate store, or the certificate has expired. "
             "Please look at the documentation on how to create local certificate store for a private CA.");

    return FALSE;
}

static gint
receive_channel_data (freerdp *instance, int channelId,
                      UINT8 *data, int size, int flags, int total_size)
{
    g_debug ("%s", G_STRLOC);

    return freerdp_channels_data (instance, channelId, data, size, flags, total_size);
}

static void
context_new (freerdp *instance, rdpContext *context)
{
    g_warn_if_fail (context->channels == NULL);
    context->channels = freerdp_channels_new ();
}

static void
context_free (freerdp *instance, rdpContext *context)
{

}

static void
gb_rdp_session_init (GbRdpSession *self)
{
    GbRdpSessionPrivate *priv;
    freerdp* instance;

    self->priv = priv =
        G_TYPE_INSTANCE_GET_PRIVATE (self, GB_RDP_TYPE_SESSION, GbRdpSessionPrivate);

    freerdp_channels_global_init ();
    instance = priv->instance = freerdp_new ();
    instance->PreConnect = pre_connect;
    instance->PostConnect = post_connect;
    instance->Authenticate = authenticate;
    instance->VerifyCertificate = verify_certificate;
    instance->ReceiveChannelData = receive_channel_data;
    instance->context_size = sizeof (GbRdpContext);
    instance->ContextNew = context_new;
    instance->ContextFree = context_free;
    freerdp_context_new (instance);

    g_return_if_fail (instance->context != NULL);
    GbRdpContext *context = (GbRdpContext *)instance->context;
    context->session = self;

    /* use freerdp sw gdi, not custom impl/hw callbacks */
    instance->settings->sw_gdi = TRUE;

    priv->source = gb_rdp_source_new (self);

    /* freerdp_parse_args */
}

static void
gb_rdp_session_constructed (GObject *object)
{
    GbRdpSession *self = GB_RDP_SESSION (object);
    GbRdpSessionPrivate *priv = self->priv;

    priv->monitors = g_array_new (FALSE, TRUE, sizeof(GbRdpMonitorConfig));

    G_OBJECT_CLASS (gb_rdp_session_parent_class)->constructed (object);
}

static void
gb_rdp_session_finalize (GObject *object)
{
    GbRdpSession *self = GB_RDP_SESSION (object);
    GbRdpSessionPrivate *priv = self->priv;

    if (priv->instance) {
        rdpChannels *channels = priv->instance->context->channels;

        if (channels) {
            freerdp_channels_close (channels, priv->instance);
            freerdp_channels_free (channels);
            priv->instance->context->channels = NULL;
        }

        g_clear_pointer (&priv->instance, freerdp_free);
    }

    g_clear_pointer (&priv->source, g_source_unref);
    g_clear_pointer (&priv->clrconv, freerdp_clrconv_free);
    g_clear_pointer (&priv->primary, cairo_surface_destroy);
    g_clear_pointer (&priv->monitors, g_array_unref);
    g_clear_object (&priv->cursor);

    if (priv->post_connect)
        g_source_remove (priv->post_connect);

    /* FIXME freerdp_channels_global_uninit() */
    G_OBJECT_CLASS (gb_rdp_session_parent_class)->finalize (object);
}

const gchar *
gb_rdp_session_get_hostname (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    rdpSettings *settings = self->priv->instance->settings;

    return settings->hostname;
}

void
gb_rdp_session_set_hostname (GbRdpSession *self,
                             const gchar *hostname)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    g_return_if_fail (hostname != NULL);

    rdpSettings *settings = self->priv->instance->settings;

    g_free (settings->hostname);
    settings->hostname = g_strdup (hostname);
}

const gchar *
gb_rdp_session_get_username (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    rdpSettings *settings = self->priv->instance->settings;

    return settings->username;
}

void
gb_rdp_session_set_username (GbRdpSession *self,
                             const gchar *username)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    g_return_if_fail (username != NULL);

    rdpSettings *settings = self->priv->instance->settings;

    g_free (settings->username);
    settings->username = g_strdup (username);
}

const gchar *
gb_rdp_session_get_password (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    rdpSettings *settings = self->priv->instance->settings;

    return settings->password;
}

void
gb_rdp_session_set_password (GbRdpSession *self,
                             const gchar *password)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    g_return_if_fail (password != NULL);

    rdpSettings *settings = self->priv->instance->settings;

    g_free (settings->password);
    settings->password = g_strdup (password);
}

const gchar *
gb_rdp_session_get_domain (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    rdpSettings *settings = self->priv->instance->settings;

    return settings->domain;
}

void
gb_rdp_session_set_domain (GbRdpSession *self,
                             const gchar *domain)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    g_return_if_fail (domain != NULL);

    rdpSettings *settings = self->priv->instance->settings;

    g_free (settings->domain);
    settings->domain = g_strdup (domain);
}

guint16
gb_rdp_session_get_port (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), 0);

    rdpSettings *settings = self->priv->instance->settings;

    return settings->port;
}

void
gb_rdp_session_set_port (GbRdpSession *self,
                         guint16 port)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));

    rdpSettings *settings = self->priv->instance->settings;

    settings->port = port;
}

cairo_surface_t*
gb_rdp_session_get_primary (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    return self->priv->primary;
}

/**
 * gb_rdp_session_get_monitors:
 * @session:
 *
 * Returns: (transfer none): the monitor configuration
 **/
GArray*
gb_rdp_session_get_monitors (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    return self->priv->monitors;
}

void
gb_rdp_session_mouse_event (GbRdpSession *self,
                            GbRdpMouseEventFlags event,
                            guint16 x, guint16 y)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    GbRdpSessionPrivate *p = self->priv;
    g_return_if_fail (p->instance != NULL);

    rdpInput *input = p->instance->input;
    guint16 flags = 0;

    if (event & GB_RDP_MOUSE_EVENT_MOVE)
        flags |= PTR_FLAGS_MOVE;
    if (event & GB_RDP_MOUSE_EVENT_DOWN)
        flags |= PTR_FLAGS_DOWN;
    if (event & GB_RDP_MOUSE_EVENT_WHEEL) {
        flags |= PTR_FLAGS_WHEEL;
        if (event & GB_RDP_MOUSE_EVENT_WHEEL_NEGATIVE)
            flags |= PTR_FLAGS_WHEEL_NEGATIVE | 0x0088; /* -128? */
        else
            flags |= 0x0078; /* +127? */
        g_debug ("%x" , flags);
    }
    if (event & GB_RDP_MOUSE_EVENT_BUTTON1)
        flags |= PTR_FLAGS_BUTTON1;
    if (event & GB_RDP_MOUSE_EVENT_BUTTON2)
        flags |= PTR_FLAGS_BUTTON2;
    if (event & GB_RDP_MOUSE_EVENT_BUTTON3)
        flags |= PTR_FLAGS_BUTTON3;

    if (flags != 0)
        freerdp_input_send_mouse_event (input, flags, x, y);
}

void
gb_rdp_session_send_key (GbRdpSession *self, GbRdpSendKey send, guint32 scancode)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    g_return_if_fail (scancode != RDP_SCANCODE_UNKNOWN);
    GbRdpSessionPrivate *p = self->priv;
    g_return_if_fail (p->instance != NULL);

    gboolean down = send == GB_RDP_SEND_KEY_PRESS;
    rdpInput *input = p->instance->input;

    freerdp_input_send_keyboard_event_ex (input, down, scancode);
}

void
gb_rdp_session_sync_key_modifier (GbRdpSession *self,
                                  GbRdpSyncKeyModifier locks)
{
    g_return_if_fail (GB_RDP_IS_SESSION (self));
    UINT32 state = 0;
    GbRdpSessionPrivate *p = self->priv;
    g_return_if_fail (p->instance != NULL);
    rdpInput *input = p->instance->input;

    if (locks & GB_RDP_SYNC_KEY_CAPS_LOCK)
        state |= KBD_SYNC_CAPS_LOCK;
    if (locks & GB_RDP_SYNC_KEY_NUM_LOCK)
        state |= KBD_SYNC_NUM_LOCK;
    if (locks & GB_RDP_SYNC_KEY_SCROLL_LOCK)
        state |= KBD_SYNC_SCROLL_LOCK;

    input->SynchronizeEvent (input, state);
}

/**
 * gb_rdp_session_get_cursor:
 * @session:
 *
 * Returns: (transfer none): the monitor configuration
 **/
GdkCursor*
gb_rdp_session_get_cursor (GbRdpSession *self)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (self), NULL);

    return self->priv->cursor;
}

static void
gb_rdp_session_get_property (GObject *object,
                             guint prop_id,
                             GValue *value,
                             GParamSpec *pspec)
{
    GbRdpSession *self = GB_RDP_SESSION (object);
    g_return_if_fail (self != NULL);

    switch (prop_id) {
    case PROP_HOSTNAME:
        g_value_set_string (value, gb_rdp_session_get_hostname (self));
        break;
    case PROP_PORT:
        g_value_set_uint (value, gb_rdp_session_get_port (self));
        break;
    case PROP_USERNAME:
        g_value_set_string (value, gb_rdp_session_get_username (self));
        break;
    case PROP_PASSWORD:
        g_value_set_string (value, gb_rdp_session_get_password (self));
        break;
    case PROP_DOMAIN:
        g_value_set_string (value, gb_rdp_session_get_domain (self));
        break;
    case PROP_PRIMARY:
        g_value_set_boxed (value, gb_rdp_session_get_primary (self));
        break;
    case PROP_MONITORS:
        g_value_set_boxed (value, gb_rdp_session_get_monitors (self));
        break;
    case PROP_CURSOR:
        g_value_set_object (value, gb_rdp_session_get_cursor (self));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gb_rdp_session_set_property (GObject *object,
                             guint prop_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
    GbRdpSession *self = GB_RDP_SESSION (object);
    g_return_if_fail (self != NULL);

    switch (prop_id) {
    case PROP_HOSTNAME:
        gb_rdp_session_set_hostname (self, g_value_get_string (value));
        break;
    case PROP_PORT:
        gb_rdp_session_set_port (self, g_value_get_uint (value));
        break;
    case PROP_USERNAME:
        gb_rdp_session_set_username (self, g_value_get_string (value));
        break;
    case PROP_PASSWORD:
        gb_rdp_session_set_password (self, g_value_get_string (value));
        break;
    case PROP_DOMAIN:
        gb_rdp_session_set_domain (self, g_value_get_string (value));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gb_rdp_session_class_init (GbRdpSessionClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    int major, minor, revision;

    g_type_class_add_private(klass, sizeof(GbRdpSessionPrivate));

    gobject_class->constructed = gb_rdp_session_constructed;
    gobject_class->finalize = gb_rdp_session_finalize;
    gobject_class->get_property = gb_rdp_session_get_property;
    gobject_class->set_property = gb_rdp_session_set_property;

    g_object_class_install_property (gobject_class,
                                     PROP_HOSTNAME,
                                     g_param_spec_string ("hostname",
                                                          "hostname",
                                                          "hostname",
                                                          NULL,
                                                          G_PARAM_STATIC_STRINGS |
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class,
                                     PROP_USERNAME,
                                     g_param_spec_string ("username",
                                                          "username",
                                                          "username",
                                                          NULL,
                                                          G_PARAM_STATIC_STRINGS |
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class,
                                     PROP_PASSWORD,
                                     g_param_spec_string ("password",
                                                          "password",
                                                          "password",
                                                          NULL,
                                                          G_PARAM_STATIC_STRINGS |
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class,
                                     PROP_DOMAIN,
                                     g_param_spec_string ("domain",
                                                          "domain",
                                                          "domain",
                                                          NULL,
                                                          G_PARAM_STATIC_STRINGS |
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class,
                                     PROP_PORT,
                                     g_param_spec_uint ("port",
                                                        "port",
                                                        "port",
                                                        0, G_MAXUINT16, 3389,
                                                        G_PARAM_STATIC_STRINGS |
                                                        G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class,
                                     PROP_PRIMARY,
                                     g_param_spec_boxed ("primary",
                                                         "primary",
                                                         "primary",
                                                         CAIRO_GOBJECT_TYPE_SURFACE,
                                                         G_PARAM_STATIC_STRINGS |
                                                         G_PARAM_READABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_MONITORS,
                                     g_param_spec_boxed ("monitors",
                                                         "monitors",
                                                         "monitors",
                                                         G_TYPE_ARRAY,
                                                         G_PARAM_STATIC_STRINGS |
                                                         G_PARAM_READABLE));

    g_object_class_install_property (gobject_class,
                                     PROP_CURSOR,
                                     g_param_spec_object ("cursor",
                                                          "cursor",
                                                          "cursor",
                                                          GDK_TYPE_CURSOR,
                                                          G_PARAM_STATIC_STRINGS |
                                                          G_PARAM_READABLE));

    signals[DISCONNECTED] =
        g_signal_new (I_("disconnected"),
                      G_OBJECT_CLASS_TYPE (gobject_class),
                      G_SIGNAL_RUN_LAST,
                      0, /* class handler */
                      NULL, NULL, /* accu */
                      NULL, /* marshaller */
                      G_TYPE_NONE,
                      1,
                      G_TYPE_INT);

    signals[INVALIDATE] =
        g_signal_new (I_("invalidate"),
                      G_OBJECT_CLASS_TYPE (gobject_class),
                      G_SIGNAL_RUN_LAST,
                      0, /* class handler */
                      NULL, NULL, /* accu */
                      NULL, /* marshaller */
                      G_TYPE_NONE,
                      1,
                      CAIRO_GOBJECT_TYPE_REGION);

    freerdp_get_version (&major, &minor, &revision);
    g_debug ("freerdp version %d.%d.%d", major, minor, revision);
}
