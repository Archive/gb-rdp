/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_RDP_SESSION_H_
#define GB_RDP_SESSION_H_

#include <glib-object.h>

G_BEGIN_DECLS

#define GB_RDP_TYPE_SESSION (gb_rdp_session_get_type ())
#define GB_RDP_SESSION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_RDP_TYPE_SESSION, GbRdpSession))
#define GB_RDP_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GB_RDP_TYPE_SESSION, GbRdpSessionClass))
#define GB_RDP_IS_SESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_RDP_TYPE_SESSION))
#define GB_RDP_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GB_RDP_TYPE_SESSION))
#define GB_RDP_SESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GB_RDP_TYPE_SESSION, GbRdpSessionClass))

typedef struct _GbRdpSession GbRdpSession;
typedef struct _GbRdpSessionClass GbRdpSessionClass;
typedef struct _GbRdpSessionPrivate GbRdpSessionPrivate;

struct _GbRdpSession
{
    GObject parent_instance;
    GbRdpSessionPrivate *priv;
};

struct _GbRdpSessionClass
{
    GObjectClass parent_class;
};

typedef struct _GbRdpMonitorConfig GbRdpMonitorConfig;

struct _GbRdpMonitorConfig
{
    gint id;
    cairo_rectangle_int_t rectangle;
};

typedef enum {
    GB_RDP_SEND_KEY_PRESS,
    GB_RDP_SEND_KEY_RELEASE,
} GbRdpSendKey;

typedef enum {
    GB_RDP_MOUSE_EVENT_MOVE           = 1 << 0,
    GB_RDP_MOUSE_EVENT_DOWN           = 1 << 1,
    GB_RDP_MOUSE_EVENT_WHEEL          = 1 << 2,
    GB_RDP_MOUSE_EVENT_WHEEL_NEGATIVE = 1 << 3,
    GB_RDP_MOUSE_EVENT_BUTTON1        = 1 << 4,
    GB_RDP_MOUSE_EVENT_BUTTON2        = 1 << 5,
    GB_RDP_MOUSE_EVENT_BUTTON3        = 1 << 6,
} GbRdpMouseEventFlags;

typedef enum {
    GB_RDP_SYNC_KEY_CAPS_LOCK   = 1 << 0,
    GB_RDP_SYNC_KEY_NUM_LOCK    = 1 << 1,
    GB_RDP_SYNC_KEY_SCROLL_LOCK = 1 << 2,
} GbRdpSyncKeyModifier;

GType         gb_rdp_session_get_type               ();

GbRdpSession* gb_rdp_session_new                    (void);
void          gb_rdp_session_connect_async          (GbRdpSession *self,
                                                     GCancellable *cancellable,
                                                     GAsyncReadyCallback callback,
                                                     gpointer user_data);
gboolean      gb_rdp_session_connect_finish         (GbRdpSession *self,
                                                     GAsyncResult *result,
                                                     GError **error);
gboolean      gb_rdp_session_disconnect             (GbRdpSession *self,
                                                     GError **error);
const gchar*  gb_rdp_session_get_hostname           (GbRdpSession *self);
void          gb_rdp_session_set_hostname           (GbRdpSession *self,
                                                     const gchar *hostname);
guint16       gb_rdp_session_get_port               (GbRdpSession *self);
void          gb_rdp_session_set_port               (GbRdpSession *self,
                                                     guint16 port);
const gchar*  gb_rdp_session_get_username           (GbRdpSession *self);
void          gb_rdp_session_set_username           (GbRdpSession *self,
                                                     const gchar *username);
const gchar*  gb_rdp_session_get_password           (GbRdpSession *self);
void          gb_rdp_session_set_password           (GbRdpSession *self,
                                                     const gchar *password);
const gchar*  gb_rdp_session_get_domain             (GbRdpSession *self);
void          gb_rdp_session_set_domain             (GbRdpSession *self,
                                                     const gchar *domain);
cairo_surface_t* gb_rdp_session_get_primary         (GbRdpSession *self);
GArray*       gb_rdp_session_get_monitors           (GbRdpSession *self);
void          gb_rdp_session_send_key               (GbRdpSession *self,
                                                     guint32 scancode,
                                                     GbRdpSendKey send);
void          gb_rdp_session_sync_key_modifier      (GbRdpSession *self,
                                                     GbRdpSyncKeyModifier locks);
void          gb_rdp_session_mouse_event            (GbRdpSession *self,
                                                     GbRdpMouseEventFlags flags,
                                                     guint16 x,
                                                     guint16 y);
GdkCursor*    gb_rdp_session_get_cursor             (GbRdpSession *self);

G_END_DECLS

#endif /* GB_RDP_SESSION_H_ */
