/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <string.h>
#include <math.h>
#include <libgsystem/libgsystem.h>

#include "gb-rdp-priv.h"
#include "gb-rdp-util-priv.h"
#include "gb-rdp-widget.h"

G_DEFINE_TYPE(GbRdpWidget, gb_rdp_widget, GTK_TYPE_DRAWING_AREA)

#define GB_RDP_WIDGET_GET_PRIVATE(obj)                                  \
    (G_TYPE_INSTANCE_GET_PRIVATE((obj), GB_RDP_TYPE_WIDGET, GbRdpWidgetPrivate))

struct _GbRdpWidgetPrivate {
    GbRdpSession           *session;
    gint                    monitor_id;
    cairo_rectangle_int_t   area;
    gboolean                ready;
    gboolean                upscale;
    gboolean                downscale;
    gboolean                has_pointer;
    gboolean                keyboard_have_focus;
    gboolean                keyboard_grab_enable;
    gboolean                keyboard_grab_active;
#ifdef WIN32
    HANDLE                  keyboard_hook;
#endif
};

#ifdef WIN32
static HWND focus_window = NULL;
#endif

/* Properties */
enum {
    PROP_0,
    PROP_MONITOR_ID,
    PROP_SESSION,
    PROP_READY,
    PROP_UPSCALE,
    PROP_DOWNSCALE,
    PROP_KEYBOARD_GRAB_ENABLE,
    PROP_KEYBOARD_GRAB_ACTIVE,
};

enum {
    LAST_SIGNAL,
};

/* static guint signals[LAST_SIGNAL]; */

/**
 * gb_rdp_widget_get_session:
 * @widget:
 *
 * Returns: (transfer none): the monitor configuration
 **/
GbRdpSession *
gb_rdp_widget_get_session (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), NULL);

    return self->priv->session;
}

gboolean
gb_rdp_widget_get_keyboard_grab_active (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), FALSE);

    return self->priv->keyboard_grab_active;
}

gboolean
gb_rdp_widget_get_keyboard_grab_enable (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), FALSE);

    return self->priv->keyboard_grab_enable;
}

int
gb_rdp_widget_get_monitor_id (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), -1);

    return self->priv->monitor_id;
}

gboolean
gb_rdp_widget_get_ready (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), -1);

    return self->priv->ready;
}

static void
set_ready (GbRdpWidget *self, gboolean ready)
{
    GbRdpWidgetPrivate *p = self->priv;

    if (p->ready == ready)
        return;

    if (ready && gtk_widget_get_window (GTK_WIDGET (self)))
        gtk_widget_queue_draw (GTK_WIDGET (self));

    p->ready = ready;
    g_object_notify (G_OBJECT (self), "ready");
}

static void
update_size_request (GbRdpWidget *self)
{
    GbRdpWidgetPrivate *p = self->priv;
    gint reqwidth, reqheight;

    if (p->downscale) {
        reqwidth = 320;
        reqheight = 240;
    } else {
        reqwidth = p->area.width;
        reqheight = p->area.height;
    }

    gtk_widget_set_size_request (GTK_WIDGET (self), reqwidth, reqheight);
}

static void
update_area (GbRdpWidget *self,
             gint x, gint y, gint width, gint height)
{
    GbRdpWidgetPrivate *p = self->priv;
    cairo_surface_t *surface = gb_rdp_session_get_primary (p->session);
    GdkRectangle primary = {
        .x = 0,
        .y = 0,
        .width = cairo_image_surface_get_width (surface),
        .height = cairo_image_surface_get_height (surface)
    };
    GdkRectangle area = {
        .x = x,
        .y = y,
        .width = width,
        .height = height
    };

    g_debug ("update area, primary: %dx%d, area: +%d+%d %dx%d",
             primary.width, primary.height, area.x, area.y, area.width, area.height);

    if (!gdk_rectangle_intersect (&primary, &area, &area)) {
        g_warning ("The monitor area is not intersecting primary surface");
        memset (&p->area, '\0', sizeof (p->area));
        set_ready (self, FALSE);
        return;
    }

    p->area = area;
    update_size_request (self);

    set_ready (self, TRUE);
}

static void
update_monitor_area (GbRdpWidget *self)
{
    GbRdpWidgetPrivate *p = self->priv;
    cairo_surface_t *surface = gb_rdp_session_get_primary (p->session);

    g_return_if_fail (surface != NULL);
    if (p->monitor_id < 0)
        goto whole;

whole:
    /* display whole surface */
    update_area (self, 0, 0,
                 cairo_image_surface_get_width (surface),
                 cairo_image_surface_get_height (surface));
    /* set_ready (self, TRUE); */
}

gboolean
gb_rdp_widget_get_downscale (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), FALSE);

    return self->priv->downscale;
}

void
gb_rdp_widget_set_downscale (GbRdpWidget *self, gboolean enable)
{
    g_return_if_fail (GB_RDP_IS_WIDGET (self));

    self->priv->downscale = enable;
    gtk_widget_queue_draw (GTK_WIDGET (self));
    update_size_request (self);
}

gboolean
gb_rdp_widget_get_upscale (GbRdpWidget *self)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (self), FALSE);

    return self->priv->upscale;
}

void
gb_rdp_widget_set_upscale (GbRdpWidget *self, gboolean enable)
{
    g_return_if_fail (GB_RDP_IS_WIDGET (self));

    self->priv->upscale = enable;
    gtk_widget_queue_draw (GTK_WIDGET (self));
    update_size_request (self);
}

static void
get_property (GObject    *object,
              guint       prop_id,
              GValue     *value,
              GParamSpec *pspec)
{
    g_return_if_fail (GB_RDP_IS_WIDGET (object));
    GbRdpWidget *self = GB_RDP_WIDGET (object);

    switch (prop_id) {
    case PROP_SESSION:
        g_value_set_object (value, gb_rdp_widget_get_session (self));
        break;
    case PROP_MONITOR_ID:
        g_value_set_int (value, gb_rdp_widget_get_monitor_id (self));
        break;
    case PROP_READY:
        g_value_set_boolean (value, gb_rdp_widget_get_ready (self));
        break;
    case PROP_DOWNSCALE:
        g_value_set_boolean (value, gb_rdp_widget_get_downscale (self));
        break;
    case PROP_UPSCALE:
        g_value_set_boolean (value, gb_rdp_widget_get_upscale (self));
        break;
    case PROP_KEYBOARD_GRAB_ACTIVE:
        g_value_set_boolean (value, gb_rdp_widget_get_keyboard_grab_active (self));
        break;
    case PROP_KEYBOARD_GRAB_ENABLE:
        g_value_set_boolean (value, gb_rdp_widget_get_keyboard_grab_enable (self));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
set_property (GObject      *object,
              guint         prop_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    g_return_if_fail (GB_RDP_IS_WIDGET (object));
    GbRdpWidget *self = GB_RDP_WIDGET (object);
    GbRdpWidgetPrivate *p = self->priv;

    switch (prop_id) {
    case PROP_SESSION:
        g_warn_if_fail (p->session == NULL);
        p->session = g_value_dup_object (value);
        break;
    case PROP_MONITOR_ID:
        p->monitor_id = g_value_get_int (value);
        update_monitor_area (self);
        break;
    case PROP_UPSCALE:
        gb_rdp_widget_set_upscale (self, g_value_get_boolean (value));
        break;
    case PROP_DOWNSCALE:
        gb_rdp_widget_set_downscale (self, g_value_get_boolean (value));
        break;
    case PROP_KEYBOARD_GRAB_ENABLE:
        gb_rdp_widget_set_keyboard_grab_enable (self, g_value_get_boolean (value));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gb_rdp_widget_init (GbRdpWidget *self)
{
    GtkWidget *widget = GTK_WIDGET (self);
    GbRdpWidgetPrivate *p;

    p = self->priv = GB_RDP_WIDGET_GET_PRIVATE (self);
    p->monitor_id = -1;

    gtk_widget_add_events (widget,
                           GDK_STRUCTURE_MASK |
                           GDK_POINTER_MOTION_MASK |
                           GDK_BUTTON_PRESS_MASK |
                           GDK_BUTTON_RELEASE_MASK |
                           GDK_BUTTON_MOTION_MASK |
                           GDK_ENTER_NOTIFY_MASK |
                           GDK_LEAVE_NOTIFY_MASK |
                           GDK_KEY_PRESS_MASK |
                           GDK_SCROLL_MASK);
    gtk_widget_set_double_buffered (widget, FALSE);
    gtk_widget_set_can_focus (widget, TRUE);
    gtk_widget_set_has_window (widget, TRUE);
}

static void
finalize (GObject *obj)
{
    G_OBJECT_CLASS (gb_rdp_widget_parent_class)->finalize (obj);
}

static void
dispose (GObject *obj)
{
    GbRdpWidget *self = GB_RDP_WIDGET(obj);
    GbRdpWidgetPrivate *p = self->priv;

    g_clear_object (&p->session);

    G_OBJECT_CLASS (gb_rdp_widget_parent_class)->dispose (obj);
}

#if GTK_CHECK_VERSION (2, 91, 0)
static inline void gdk_drawable_get_size (GdkWindow *w, gint *ww, gint *wh)
{
       *ww = gdk_window_get_width(w);
       *wh = gdk_window_get_height(w);
}
#endif

static void
get_scaling (GbRdpWidget *self, double *s_out,
             int *x_out, int *y_out,
             int *w_out, int *h_out)
{
    GbRdpWidgetPrivate *p = self->priv;
    int fbw = p->area.width, fbh = p->area.height;
    int ww, wh;
    int x, y, w, h;
    double s;

    if (gtk_widget_get_realized (GTK_WIDGET (self)))
        gdk_drawable_get_size (gtk_widget_get_window (GTK_WIDGET (self)), &ww, &wh);
    else {
        ww = fbw;
        wh = fbh;
    }

    s = MIN ((double)ww / (double)fbw, (double)wh / (double)fbh);

    if ((!p->upscale && s > 1.0) ||
        (!p->downscale && s < 1.0))
        s = 1.0;

    /* Round to int size */
    w = floor (fbw * s + 0.5);
    h = floor (fbh * s + 0.5);

    /* Center the display */
    x = (ww - w) / 2;
    y = (wh - h) / 2;

    if (s_out)
        *s_out = s;
    if (w_out)
        *w_out = w;
    if (h_out)
        *h_out = h;
    if (x_out)
        *x_out = x;
    if (y_out)
        *y_out = y;
}

static void
invalidate (GbRdpSession *session,
            const cairo_region_t *inv,
            GbRdpWidget *self)
{
    if (!gtk_widget_get_window (GTK_WIDGET (self)))
        return;

    GbRdpWidgetPrivate *p = self->priv;
    gb_rdp_lregion cairo_region_t *region = cairo_region_copy (inv);
    gb_rdp_lregion cairo_region_t *region_scale = cairo_region_create ();
    double s;
    int x, y;

    g_return_if_fail (cairo_region_intersect_rectangle (region, &p->area) == 0);

    get_scaling (self, &s, &x, &y, NULL, NULL);
    int i, num = cairo_region_num_rectangles (region);

    for (i = 0; i < num; i++) {
        cairo_rectangle_int_t rect;

        cairo_region_get_rectangle (region, i, &rect);
        rect.x = floor ((rect.x - p->area.x) * s);
        rect.y = floor ((rect.y - p->area.y) * s);
        rect.width = ceil (rect.width * s);
        rect.height = ceil (rect.height * s);
        cairo_region_union_rectangle (region_scale, &rect);
    }

    cairo_region_translate (region_scale, x, y);
    gtk_widget_queue_draw_region (GTK_WIDGET (self), region_scale);
}

static void
cursor (GbRdpSession *session, GParamSpec *pspec, GbRdpWidget *self)
{
    GdkWindow *window = GDK_WINDOW (gtk_widget_get_window (GTK_WIDGET (self)));
    GdkCursor *cursor = gb_rdp_session_get_cursor (session);

    if (gdk_window_get_cursor (window) != cursor)
        gdk_window_set_cursor (window, cursor);
}

static GObject *
constructor (GType                  gtype,
             guint                  n_properties,
             GObjectConstructParam *properties)
{
    GObject *obj;
    GbRdpWidget *self;
    GbRdpWidgetPrivate *p;

    obj = G_OBJECT_CLASS (gb_rdp_widget_parent_class)->
        constructor (gtype, n_properties, properties);

    self = GB_RDP_WIDGET (obj);
    p = self->priv;

    if (!p->session)
        g_error ("GbRdpWidget constructed without a session");

    gb_rdp_signal_connect_object (p->session, "invalidate",
                                  G_CALLBACK (invalidate), self, 0);
    gb_rdp_signal_connect_object (p->session, "notify::cursor",
                                  G_CALLBACK (cursor), self, 0);

    return obj;
}

static gboolean
draw (GtkWidget *widget, cairo_t *cr)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);

    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;

    cairo_surface_t *primary = gb_rdp_session_get_primary (p->session);
    cairo_rectangle_int_t rect;
    cairo_region_t *region;
    double s;
    int x, y;
    int ww, wh;
    int w, h;

    get_scaling (self, &s, &x, &y, &w, &h);
    gdk_drawable_get_size (gtk_widget_get_window (widget), &ww, &wh);

    /* We need to paint the bg color around the image */
    rect.x = 0;
    rect.y = 0;
    rect.width = ww;
    rect.height = wh;
    region = cairo_region_create_rectangle (&rect);

    /* Optionally cut out the inner area where the pixmap
       will be drawn. This avoids 'flashing' since we're
       not double-buffering. */
    if (primary) {
        rect.x = x;
        rect.y = y;
        rect.width = w;
        rect.height = h;
        cairo_region_subtract_rectangle (region, &rect);
    }

    gdk_cairo_region (cr, region);
    cairo_region_destroy (region);

    /* Need to set a real solid color, because the default is usually
       transparent these days, and non-double buffered windows can't
       render transparently */
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_fill (cr);

    if (!primary)
        return TRUE;

    /* Draw the display */
    cairo_translate (cr, x, y);
    cairo_rectangle (cr, 0, 0, w, h);
    cairo_scale (cr, s, s);
    cairo_translate (cr, -p->area.x, -p->area.y);
    cairo_set_source_surface (cr, primary, 0, 0);
    cairo_fill (cr);

    return TRUE;
}

static void
send_key (GbRdpWidget *self, GbRdpSendKey send, guint16 keycode)
{
    GbRdpWidgetPrivate *p = self->priv;
    RDP_SCANCODE scancode = freerdp_keyboard_get_rdp_scancode_from_x11_keycode (keycode);

    gb_rdp_session_send_key (p->session, send, scancode);
}

static gboolean
key_event (GtkWidget *widget, GdkEventKey *key)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    guint16 keycode = key->hardware_keycode;

    switch (key->type) {
    case GDK_KEY_PRESS:
        send_key (self, GB_RDP_SEND_KEY_PRESS, keycode);
        break;
    case GDK_KEY_RELEASE:
        send_key (self, GB_RDP_SEND_KEY_RELEASE, keycode);
        break;
    default:
        g_warn_if_reached ();
        break;
    }

    return TRUE;
}

#ifdef WIN32
static LRESULT CALLBACK
keyboard_hook_cb (int code, WPARAM wparam, LPARAM lparam)
{
    if  (focus_window && code == HC_ACTION) {
        KBDLLHOOKSTRUCT *hooked = (KBDLLHOOKSTRUCT*)lparam;
        DWORD dwmsg = (hooked->flags << 24) | (hooked->scanCode << 16) | 1;

        if (hooked->vkCode == VK_NUMLOCK || hooked->vkCode == VK_RSHIFT) {
            dwmsg &= ~(1 << 24);
            SendMessage (focus_window, wparam, hooked->vkCode, dwmsg);
        }
        switch (hooked->vkCode) {
        case VK_CAPITAL:
        case VK_SCROLL:
        case VK_NUMLOCK:
        case VK_LSHIFT:
        case VK_RSHIFT:
        case VK_LCONTROL:
        case VK_RCONTROL:
        case VK_LMENU:
        case VK_RMENU:
            break;
        default:
            SendMessage (focus_window, wparam, hooked->vkCode, dwmsg);
            return 1;
        }
    }
    return CallNextHookEx (NULL, code, wparam, lparam);
}
#endif

static void
try_keyboard_grab (GbRdpWidget *self)
{
    GtkWidget *widget = GTK_WIDGET (self);
    GbRdpWidgetPrivate *p = self->priv;
    GdkGrabStatus status;

    if (g_getenv("GB_RDP_NOGRAB"))
        return;

    if (!p->keyboard_grab_enable)
        return;
    if (p->keyboard_grab_active)
        return;
    if (!p->keyboard_have_focus)
        return;
    if (!p->has_pointer)
        return;

    g_return_if_fail (gtk_widget_is_focus (widget));

    g_debug ("grab keyboard");
    gtk_widget_grab_focus (widget);

#ifdef WIN32
    if (p->keyboard_hook == NULL)
        p->keyboard_hook = SetWindowsHookEx (WH_KEYBOARD_LL, keyboard_hook_cb,
                                             GetModuleHandle (NULL), 0);
    g_warn_if_fail (p->keyboard_hook != NULL);
#endif
    status = gdk_keyboard_grab (gtk_widget_get_window (widget),
                                FALSE, GDK_CURRENT_TIME);
    if (status != GDK_GRAB_SUCCESS) {
        g_warning ("keyboard grab failed %d", status);
        p->keyboard_grab_active = FALSE;
    } else {
        p->keyboard_grab_active = TRUE;
    }

    g_object_notify (G_OBJECT (self), "keyboard-grab-active");
}

static void
try_keyboard_ungrab (GbRdpWidget *self)
{
    GbRdpWidgetPrivate *p = self->priv;

    if (!p->keyboard_grab_active)
        return;

    g_debug ("ungrab keyboard");

    gdk_keyboard_ungrab (GDK_CURRENT_TIME);
#ifdef WIN32
    if (d->keyboard_hook != NULL) {
        UnhookWindowsHookEx (d->keyboard_hook);
        d->keyboard_hook = NULL;
    }
#endif
    p->keyboard_grab_active = FALSE;

    g_object_notify (G_OBJECT (self), "keyboard-grab-active");
}

static void
update_keyboard_grab (GbRdpWidget *self)
{
    GbRdpWidgetPrivate *p = self->priv;

    if (p->keyboard_grab_enable)
        try_keyboard_grab (self);
    else
        try_keyboard_ungrab (self);
}

void
gb_rdp_widget_set_keyboard_grab_enable (GbRdpWidget *self, gboolean enable)
{
    g_return_if_fail (GB_RDP_IS_WIDGET (self));

    self->priv->keyboard_grab_enable = enable;
    update_keyboard_grab (self);
}

static gboolean
enter_event (GtkWidget *widget, GdkEventCrossing *crossing G_GNUC_UNUSED)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);

    self->priv->has_pointer = TRUE;
    try_keyboard_grab (self);

    return TRUE;
}

static gboolean
leave_event (GtkWidget *widget, GdkEventCrossing *crossing G_GNUC_UNUSED)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);

    self->priv->has_pointer = FALSE;
    try_keyboard_ungrab (self);

    return TRUE;
}

static void
update_keyboard_focus (GbRdpWidget *self, gboolean state)
{
    GbRdpWidgetPrivate *p = self->priv;

    p->keyboard_have_focus = state;
}

static void
sync_keyboard_lock_modifiers (GbRdpWidget *self)
{
    GbRdpSyncKeyModifier locks = 0;
    GdkModifierType state;
    GdkDeviceManager *manager =
        gdk_display_get_device_manager (gdk_display_get_default ());
    GdkDevice *keyboard = gdk_device_manager_get_client_pointer (manager);
    gdk_window_get_device_position (gdk_get_default_root_window (),
                                    keyboard, NULL, NULL, &state);

    if (state & GDK_LOCK_MASK)
        locks |= GB_RDP_SYNC_KEY_CAPS_LOCK;
    if (state & GDK_MOD2_MASK)
        locks |= GB_RDP_SYNC_KEY_NUM_LOCK;
    if (state & GDK_MOD5_MASK)
        locks |= GB_RDP_SYNC_KEY_SCROLL_LOCK;

    gb_rdp_session_sync_key_modifier (self->priv->session, locks);
}

static gboolean
focus_in_event (GtkWidget *widget, GdkEventFocus *focus G_GNUC_UNUSED)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;

    /*
     * Ignore focus in when we already have the focus
     * (this happens when doing an ungrab from the leave_event callback).
     */
    if (p->keyboard_have_focus)
        return TRUE;

    /* release_keys (self); */
    sync_keyboard_lock_modifiers (self);
    update_keyboard_focus (self, TRUE);
    try_keyboard_grab (self);
#ifdef WIN32
    focus_window = GDK_WINDOW_HWND (gtk_widget_get_window (widget));
    g_return_val_if_fail (focus_window != NULL, TRUE);
#endif

    return TRUE;
}

static gboolean
focus_out_event (GtkWidget *widget, GdkEventFocus *focus G_GNUC_UNUSED)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;

    /*
     * Ignore focus out after a keyboard grab
     * (this happens when doing the grab from the enter_event callback).
     */
    if (!p->keyboard_grab_active)
        return TRUE;

    /* release_keys (self); */
    update_keyboard_focus (self, FALSE);

    return TRUE;
}

static gboolean
transform_input (GbRdpWidget *self,
                 double window_x, double window_y,
                 int *input_x, int *input_y)
{
    GbRdpWidgetPrivate *p = self->priv;
    int display_x, display_y, display_w, display_h;
    double is;

    get_scaling (self, NULL,
                 &display_x, &display_y,
                 &display_w, &display_h);

    /* For input we need a different scaling factor in order to
       be able to reach the full width of a display. For instance, consider
       a display of 100 pixels showing in a window 10 pixels wide. The normal
       scaling factor here would be 100/10==10, but if you then take the largest
       possible window coordinate, i.e. 9 and multiply by 10 you get 90, not 99,
       which is the max display coord.

       If you want to be able to reach the last pixel in the window you need
       max_window_x * input_scale == max_display_x, which is
       (window_width - 1) * input_scale == (display_width - 1)

       Note, this is the inverse of s (i.e. s ~= 1/is) as we're converting the
       coordinates in the inverse direction (window -> display) as the fb size
       (display -> window).
    */
    is = (double)(p->area.width-1) / (double)(display_w-1);

    window_x -= display_x;
    window_y -= display_y;

    *input_x = floor (window_x * is);
    *input_y = floor (window_y * is);

    if (*input_x >= 0 && *input_x < p->area.width &&
        *input_y >= 0 && *input_y < p->area.height) {
        *input_x += p->area.x;
        *input_y += p->area.y;
        return TRUE;
    }

    return FALSE;
}

static gboolean
motion_event (GtkWidget *widget, GdkEventMotion *event)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;
    int x, y;

    if (transform_input (self, event->x, event->y, &x, &y))
        gb_rdp_session_mouse_event (p->session, GB_RDP_MOUSE_EVENT_MOVE, x, y);

    return TRUE;
}

static gboolean
button_event (GtkWidget *widget, GdkEventButton *event)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;
    guint16 flags = 0;
    int x, y;

    if ((event->button < 1) || (event->button > 3))
        return FALSE;

    if ((event->type != GDK_BUTTON_PRESS) && (event->type != GDK_BUTTON_RELEASE))
        return FALSE;

    if (!transform_input (self, event->x, event->y, &x, &y))
        return FALSE;

    if (event->type == GDK_BUTTON_PRESS)
        flags = GB_RDP_MOUSE_EVENT_DOWN;
    if (event->button == 1)
        flags |= GB_RDP_MOUSE_EVENT_BUTTON1;
    if (event->button == 2)
        flags |= GB_RDP_MOUSE_EVENT_BUTTON3;
    if (event->button == 3)
        flags |= GB_RDP_MOUSE_EVENT_BUTTON2;

    gb_rdp_session_mouse_event (p->session, flags, x, y);

    return TRUE;
}

static gboolean
configure_event (GtkWidget *widget, GdkEventConfigure *conf)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);

    return TRUE;
}

static gboolean
scroll_event (GtkWidget *widget, GdkEventScroll *event)
{
    g_return_val_if_fail (GB_RDP_IS_WIDGET (widget), TRUE);
    GbRdpWidget *self = GB_RDP_WIDGET (widget);
    GbRdpWidgetPrivate *p = self->priv;
    guint16 flags = GB_RDP_MOUSE_EVENT_WHEEL;
    int x, y;

    if (!transform_input (self, event->x, event->y, &x, &y))
        return FALSE;

    switch (event->direction) {
    case GDK_SCROLL_UP:
        break;
    case GDK_SCROLL_DOWN:
        flags |= GB_RDP_MOUSE_EVENT_WHEEL_NEGATIVE;
        break;
    case GDK_SCROLL_SMOOTH:
        g_debug ("scroll smooth unhandled");
    default:
        return FALSE;
    }

    gb_rdp_session_mouse_event (p->session, flags, x, y);

    return TRUE;
}

static void
realize (GtkWidget *widget)
{
    GTK_WIDGET_CLASS (gb_rdp_widget_parent_class)->realize (widget);
}

static void
unrealize (GtkWidget *widget)
{
    GTK_WIDGET_CLASS (gb_rdp_widget_parent_class)->unrealize (widget);
}

static void
gb_rdp_widget_class_init (GbRdpWidgetClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *gtkwidget_class = GTK_WIDGET_CLASS (klass);

    g_type_class_add_private (klass, sizeof (GbRdpWidgetPrivate));

    gobject_class->constructor = constructor;
    gobject_class->dispose = dispose;
    gobject_class->finalize = finalize;
    gobject_class->get_property = get_property;
    gobject_class->set_property = set_property;

    gtkwidget_class->draw = draw;
    gtkwidget_class->key_press_event = key_event;
    gtkwidget_class->key_release_event = key_event;
    gtkwidget_class->enter_notify_event = enter_event;
    gtkwidget_class->leave_notify_event = leave_event;
    gtkwidget_class->focus_in_event = focus_in_event;
    gtkwidget_class->focus_out_event = focus_out_event;
    gtkwidget_class->motion_notify_event = motion_event;
    gtkwidget_class->button_press_event = button_event;
    gtkwidget_class->button_release_event = button_event;
    gtkwidget_class->configure_event = configure_event;
    gtkwidget_class->scroll_event = scroll_event;
    gtkwidget_class->realize = realize;
    gtkwidget_class->unrealize = unrealize;

    g_object_class_install_property (gobject_class,
                                     PROP_SESSION,
                                     g_param_spec_object ("session",
                                                          "Session",
                                                          "GbRdpSession",
                                                          GB_RDP_TYPE_SESSION,
                                                          G_PARAM_CONSTRUCT_ONLY |
                                                          G_PARAM_READWRITE |
                                                          G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_MONITOR_ID,
                                     g_param_spec_int ("monitor-id",
                                                       "Monitor ID",
                                                       "Select monitor ID",
                                                       -1, G_MAXINT, 0,
                                                       G_PARAM_READWRITE |
                                                       G_PARAM_CONSTRUCT |
                                                       G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_READY,
                                     g_param_spec_boolean ("ready",
                                                           "Ready",
                                                           "Ready to display",
                                                           FALSE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_UPSCALE,
                                     g_param_spec_boolean ("upscale",
                                                           "upscale",
                                                           "upscale",
                                                           FALSE,
                                                           G_PARAM_READWRITE |
                                                           G_PARAM_CONSTRUCT |
                                                           G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_DOWNSCALE,
                                     g_param_spec_boolean ("downscale",
                                                           "downscale",
                                                           "downscale",
                                                           TRUE,
                                                           G_PARAM_READWRITE |
                                                           G_PARAM_CONSTRUCT |
                                                           G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_KEYBOARD_GRAB_ENABLE,
                                     g_param_spec_boolean ("keyboard-grab-enable",
                                                           "keyboard grab enable",
                                                           "Grab keyboard",
                                                           TRUE,
                                                           G_PARAM_READWRITE |
                                                           G_PARAM_CONSTRUCT |
                                                           G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class,
                                     PROP_KEYBOARD_GRAB_ACTIVE,
                                     g_param_spec_boolean ("keyboard-grab-active",
                                                           "keyboard grab active",
                                                           "Keyboard is grabbed",
                                                           FALSE,
                                                           G_PARAM_READABLE |
                                                           G_PARAM_STATIC_STRINGS));
}


GbRdpWidget*
gb_rdp_widget_new (GbRdpSession *session, int monitor_id)
{
    g_return_val_if_fail (GB_RDP_IS_SESSION (session), NULL);

    return g_object_new (GB_RDP_TYPE_WIDGET,
                         "session", session,
                         "monitor-id", monitor_id,
                         NULL);
}
