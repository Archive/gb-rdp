/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_RDP_PRIV_H_
#define GB_RDP_PRIV_H_

#include <stdint.h>
#include <freerdp/channels/channels.h>
#include <freerdp/constants.h>
#include <freerdp/freerdp.h>
#include <freerdp/gdi/gdi.h>
#include <freerdp/locale/keyboard.h>
/* #include <freerdp/plugins/cliprdr.h> */
#include <freerdp/keyboard_scancode.h>
#include <freerdp/utils/args.h>
#include <freerdp/utils/event.h>
#include <freerdp/utils/memory.h>
#include <freerdp/utils/semaphore.h>

#define I_(string) g_intern_static_string (string)

#endif /* GB_RDP_PRIV_H_ */
