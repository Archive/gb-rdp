/*
 * Copyright (C) 2012 Marc-André Lureau <marcandre.lureau@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_RDP_WIDGET_H_
#define GB_RDP_WIDGET_H_

#include <gtk/gtk.h>

#include "gb-rdp-enums.h"
#include "gb-rdp-util.h"
#include "gb-rdp-session.h"

G_BEGIN_DECLS

#define GB_RDP_TYPE_WIDGET            (gb_rdp_widget_get_type())
#define GB_RDP_WIDGET(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GB_RDP_TYPE_WIDGET, GbRdpWidget))
#define GB_RDP_WIDGET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GB_RDP_TYPE_WIDGET, GbRdpWidgetClass))
#define GB_RDP_IS_WIDGET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GB_RDP_TYPE_WIDGET))
#define GB_RDP_IS_WIDGET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GB_RDP_TYPE_WIDGET))
#define GB_RDP_WIDGET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GB_RDP_TYPE_WIDGET, GbRdpWidgetClass))

typedef struct _GbRdpWidget GbRdpWidget;
typedef struct _GbRdpWidgetClass GbRdpWidgetClass;
typedef struct _GbRdpWidgetPrivate GbRdpWidgetPrivate;

struct _GbRdpWidget {
    GtkDrawingArea parent;

    GbRdpWidgetPrivate *priv;
};

struct _GbRdpWidgetClass {
    GtkDrawingAreaClass parent_class;

    /*< private >*/
    /*
     * If adding fields to this struct, remove corresponding
     * amount of padding to avoid changing overall struct size
     */
    gchar _reserved[GB_RDP_RESERVED_PADDING];
};

GType	       gb_rdp_widget_get_type (void);

GbRdpWidget*   gb_rdp_widget_new (GbRdpSession *session, int monitor_id);
GbRdpSession*  gb_rdp_widget_get_session (GbRdpWidget *widget);
int            gb_rdp_widget_get_monitor_id (GbRdpWidget *widget);
gboolean       gb_rdp_widget_get_ready (GbRdpWidget *widget);
gboolean       gb_rdp_widget_get_upscale (GbRdpWidget *widget);
void           gb_rdp_widget_set_upscale (GbRdpWidget *widget, gboolean enable);
gboolean       gb_rdp_widget_get_downscale (GbRdpWidget *widget);
void           gb_rdp_widget_set_downscale (GbRdpWidget *widget, gboolean enable);
gboolean       gb_rdp_widget_get_keyboard_grab_active (GbRdpWidget *widget);
gboolean       gb_rdp_widget_get_keyboard_grab_enable (GbRdpWidget *widget);
void           gb_rdp_widget_set_keyboard_grab_enable (GbRdpWidget *widget, gboolean enable);


G_END_DECLS

#endif /* GB_RDP_WIDGET_H_ */
